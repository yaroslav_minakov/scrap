package com.scrap.model.element;


import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.ScreenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.image.BufferedImage;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DoubleElementTest {

	@Autowired
	private ScreenService screenService;

	@Autowired
	private RecogniseService recogniseService;

	private BufferedImage image;

	@Before
	public void getImage() {
		image = screenService.getImageFromResources("test.jpg");
	}

	@Test
	public void getValue() {
		DoubleElement element = new DoubleElement(new Area(190, 390, 1, 40));
		assertEquals(7.17d, element.getValue(image, recogniseService).doubleValue(), 0);
	}

}