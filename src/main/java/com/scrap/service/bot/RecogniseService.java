package com.scrap.service.bot;

import lombok.extern.slf4j.Slf4j;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.awt.image.BufferedImage;

import static net.sourceforge.tess4j.ITessAPI.TessPageSegMode.PSM_SINGLE_LINE;

@Service
@Slf4j
@Scope("singleton")
public class RecogniseService {

	private Tesseract tesseract;

	@PostConstruct
	private void init() {
		tesseract = new Tesseract();
		tesseract.setLanguage("scrap");
		tesseract.setTessVariable("tessedit_char_whitelist", RecogniseType.COMBINE.getValue());
		tesseract.setTessVariable("debug_file", "/dev/null");
		tesseract.setPageSegMode(PSM_SINGLE_LINE);
	}

	public String recognise(BufferedImage image, RecogniseType recogniseType) {
		String result = null;
		try {
			tesseract.setTessVariable("tessedit_char_whitelist", recogniseType.getValue());
			result = tesseract.doOCR(image);
		} catch (TesseractException e) {
			log.error("Can't recognise image", e);
		}
		return result;
	}

	public String recognise(BufferedImage image) {
		return recognise(image, RecogniseType.COMBINE);
	}
}
