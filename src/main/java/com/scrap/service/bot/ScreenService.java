package com.scrap.service.bot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.RenderedImage;
import java.awt.image.RescaleOp;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

@Service
@Slf4j
@Scope("singleton")
public class ScreenService {

	@Inject
	private ClickerService clickerService;

	public BufferedImage convertImage(BufferedImage image) {
		ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
		ColorConvertOp op = new ColorConvertOp(cs, null);
		RescaleOp rescaleOp = new RescaleOp(1.5f, 15, null);
		return rescaleOp.filter(op.filter(image, null), null);
	}

	public BufferedImage getScreenShot() {
		return convertImage(clickerService.getScreenShot());
//		BufferedImage screen = getImageFromResources("pic2.jpg");
//		return convertImage(screen);
	}

	public BufferedImage getScreenShot(int x, int y, int width, int height) {
		return getArea(getScreenShot(), x, y, width, height);
	}

	public ByteArrayOutputStream getImage(RenderedImage image) {
		ByteArrayOutputStream bao = null;
		// Write to output stream
		try {
			bao = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", bao);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bao;
	}

	public BufferedImage getArea(BufferedImage image, int x, int y, int width, int height) {
		return image.getSubimage(x, y, width, height);
	}

	public BufferedImage getImageFromResources(String filename) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("img/" + filename).getFile());
		BufferedImage image = null;
		try {
			image = ImageIO.read(file);
		} catch (IOException e) {
			log.error("Can't get a screen shot");
		}
		return image;
	}
}
