package com.scrap.service.bot;

import com.scrap.util.Wait;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

@Service
@Slf4j
@Scope("singleton")
public class ClickerService {

	private Robot robot;

	private double x;

	private double y;

	@PostConstruct
	private void init() {
		System.setProperty("java.awt.headless", "false");
		try {
			robot = new Robot();
			x = 0;
			y = 0;
		} catch (AWTException e) {
			log.error("Can't initialise clicker service", e);
		}
	}

	public ClickerService moveTo(int x, int y) {

		double dx = (x - this.x) / 50;
		double dy = (y - this.y) /  50;

		for (int step = 1; step <= 50; step++) {
			Wait.mill(10);
			robot.mouseMove( (int)(this.x + dx * step), (int) (this.y + dy * step));
		}
		this.x = x;
		this.y = y;
		return this;
	}

	public ClickerService leftClick() {
		robot.mousePress(InputEvent.BUTTON1_MASK);
		Wait.mill(500);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
		return this;
	}

	public ClickerService rightClick() {
		robot.mousePress(InputEvent.BUTTON2_MASK);
		Wait.mill(500);
		robot.mouseRelease(InputEvent.BUTTON2_MASK);
		return this;
	}

	public ClickerService print(String string) {
		for (char c : string.toCharArray()) {
			robot.keyPress(c);
			robot.keyRelease(c);
			Wait.mill(100);
		}
		return this;
	}

	public ClickerService enter() {
		robot.keyPress(KeyEvent.VK_ENTER);
		return this;
	}

	public BufferedImage getScreenShot() {
		return robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	}
}
