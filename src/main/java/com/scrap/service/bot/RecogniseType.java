package com.scrap.service.bot;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@AllArgsConstructor
public enum RecogniseType {

	COMBINE("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,0123456789-"),

	UPPER("ABCDEFGHIJKLMNOPQRSTUVWXYZ.,"),

	LOWER("abcdefghijklmnopqrstuvwxyz.,"),

	LETTERS("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,"),

	NUMBERS("0123456789-."),

	NOT_DEFINED(null);

	@Getter
	private String value;

	@JsonCreator
	public static RecogniseType getByValue(String value) {
		return Arrays.stream(RecogniseType.values())
				.filter(type -> Objects.equals(type.getValue(), value))
				.findFirst()
				.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", value)));
	}

}
