package com.scrap.model.shop;

import com.scrap.model.gameface.PartTab;
import com.scrap.service.bot.RecogniseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.util.Stack;

@Component
public class PartAdapter {

	@Autowired
	private RecogniseService recogniseService;

	public Part adapt(PartTab partTab, BufferedImage image) {
			Stack<Row> sell = new Stack<>();
			partTab.getSellList().forEach(rowElement -> sell.push(new Row(rowElement.getPrice().getValue(image, recogniseService),
					rowElement.getCount().getValue(image, recogniseService))));

			Stack<Row> buy = new Stack<>();
			partTab.getBuyList().forEach(rowElement -> buy.push(new Row(rowElement.getPrice().getValue(image, recogniseService),
					rowElement.getCount().getValue(image, recogniseService))));
		return new Part(sell, buy);
	}

}
