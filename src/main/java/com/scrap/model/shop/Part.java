package com.scrap.model.shop;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Stack;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Part {

	private Stack<Row> sell;

	private Stack<Row> buy;

}
