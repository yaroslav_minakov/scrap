package com.scrap.model.element;

import com.scrap.service.bot.ClickerService;

public interface Clickable {

    void click(ClickerService clickerService);
}
