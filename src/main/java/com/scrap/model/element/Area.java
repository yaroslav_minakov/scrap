package com.scrap.model.element;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Area {

    int x;

    int y;

    int width;

    int height;

}
