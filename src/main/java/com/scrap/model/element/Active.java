package com.scrap.model.element;

import com.scrap.service.bot.RecogniseService;

import java.awt.image.BufferedImage;

public interface Active {

	boolean isActive(BufferedImage image, RecogniseService recogniseService);

}
