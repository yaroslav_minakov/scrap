package com.scrap.model.element;

import com.scrap.service.bot.ClickerService;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;

import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class ButtonElement extends ElementAbstract<String> implements Active, Clickable{

	private String name;

	private RecogniseType recogniseType;

	private Area visualArea;

	public ButtonElement(String name, Area visualArea, RecogniseType recogniseType, Area area) {
		super(area);
		this.name = name;
		this.recogniseType = recogniseType;
		this.visualArea = visualArea;
	}

	@Override
	public String getValue(BufferedImage image, RecogniseService recogniseService) {
		return recogniseService.recognise(getImage(image), recogniseType).trim();
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return Objects.equals(getValue(image, recogniseService), name);
	}

	@Override
	public void click(ClickerService clickerService) {
		int x = ThreadLocalRandom.current().nextInt(visualArea.getX(), visualArea.getX() + visualArea.getWidth());
		int y = ThreadLocalRandom.current().nextInt(visualArea.getY(), visualArea.getY() + visualArea.getHeight());
		clickerService.moveTo(x, y);
		clickerService.leftClick();
	}
}