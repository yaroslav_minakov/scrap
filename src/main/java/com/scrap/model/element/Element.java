package com.scrap.model.element;

import com.scrap.service.bot.RecogniseService;

import java.awt.image.BufferedImage;

public interface Element<T> {

	BufferedImage getImage(BufferedImage image);

	T getValue(BufferedImage image, RecogniseService recogniseService);

}
