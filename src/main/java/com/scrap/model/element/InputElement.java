package com.scrap.model.element;

import com.scrap.service.bot.ClickerService;
import com.scrap.service.bot.RecogniseService;

import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class InputElement extends ElementAbstract<String> implements Active, Clickable, Printable {

	private String name;

	private Area visualArea;

	public InputElement(String name, Area visualArea, Area area) {
		super(area);
		this.name = name;
		this.visualArea = visualArea;
	}

	@Override
	public String getValue(BufferedImage image, RecogniseService recogniseService) {
		return recogniseService.recognise(getImage(image)).trim();
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return Objects.equals(getValue(image, recogniseService), name);
	}

	@Override
	public void click(ClickerService clickerService) {
		int x = ThreadLocalRandom.current().nextInt(visualArea.getX(), visualArea.getX() + visualArea.getWidth());
		int y = ThreadLocalRandom.current().nextInt(visualArea.getY(), visualArea.getY() + visualArea.getHeight());
		clickerService.moveTo(x, y);
		clickerService.leftClick();
	}

	@Override
	public void print(String string, ClickerService clickerService) {
		clickerService.print(string);
		clickerService.enter();
	}
}