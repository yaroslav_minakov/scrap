package com.scrap.model.element;

import java.awt.image.BufferedImage;


public abstract class ElementAbstract<T> implements Element<T> {

	protected Area area;

	public ElementAbstract(Area area) {
		this.area = area;
	}

	@Override
	public BufferedImage getImage(BufferedImage image) {
		return image.getSubimage(area.getX(), area.getY(), area.getWidth(), area.getHeight());
	}
}
