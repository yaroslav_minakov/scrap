package com.scrap.model.element;

import com.scrap.service.bot.RecogniseService;

import java.awt.image.BufferedImage;

public class StringElement extends ElementAbstract<String> {

	public StringElement(Area area) {
		super(area);
	}

	@Override
	public String getValue(BufferedImage image, RecogniseService recogniseService) {
		return recogniseService.recognise(getImage(image)).trim();
	}
}