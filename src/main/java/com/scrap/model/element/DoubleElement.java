package com.scrap.model.element;

import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;

import java.awt.image.BufferedImage;
import java.util.Objects;

public class DoubleElement extends ElementAbstract<Double> {

	private static final int MAX_NUMBER_OF_ITERATION = 10;
	private static final int SYMBOL_WIDTH = 10;
	private static final int NUMBERS_AFTER_COMMA = 2;
	private static final int MIN_SYMBOLS_IN_NUMBER = 3;
	private static final String DELIMITER = ".";

	public DoubleElement(Area area) {
		super(area);
	}

	@Override
	public Double getValue(BufferedImage image, RecogniseService recogniseService) throws NumberFormatException {
		int counter = 0;
		Double result = null;
		int extend = 0;
		while (Objects.isNull(result) && counter <= MAX_NUMBER_OF_ITERATION) {
			String raw = recogniseService.recognise(getImage(image, extend), RecogniseType.NUMBERS);
			raw = raw.trim();
			if (raw.length() > MIN_SYMBOLS_IN_NUMBER
					&& raw.contains(DELIMITER)
					&& raw.substring(raw.indexOf(DELIMITER)).length() - 1 >= NUMBERS_AFTER_COMMA) {
				//trim length of raw value to NUMBERS AFTER COMMA
				if (raw.substring(raw.indexOf(DELIMITER)).length() - 1 > NUMBERS_AFTER_COMMA) {
					raw = raw.substring(0, raw.indexOf(DELIMITER) + NUMBERS_AFTER_COMMA + 1);
				}
				result = Double.valueOf(raw);
			} else {
				extend += SYMBOL_WIDTH;
			}
			counter++;
		}
		return result;
	}

	private BufferedImage getImage(BufferedImage image, int extend) {
		return image.getSubimage(area.getX(), area.getY(), area.getWidth() + extend, area.getHeight());
	}

}
