package com.scrap.model.element;

import com.scrap.service.bot.ClickerService;

public interface Printable {

	void print(String string, ClickerService clickerService);

}
