package com.scrap.model.element;

import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import org.apache.commons.lang3.math.NumberUtils;

import java.awt.image.BufferedImage;

public class IntegerElement extends ElementAbstract<Integer> {

	public IntegerElement(Area area) {
		super(area);
	}

	@Override
	public Integer getValue(BufferedImage image, RecogniseService recogniseService) {
		String raw = recogniseService.recognise(getImage(image), RecogniseType.NUMBERS).trim();
		Integer result = null;
		if (NumberUtils.isCreatable(raw) && NumberUtils.isDigits(raw)) {
			result = Integer.valueOf(raw);
		}
		return result;
	}
}