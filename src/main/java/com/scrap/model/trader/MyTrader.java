package com.scrap.model.trader;

import com.scrap.model.gameface.Interface;
import com.scrap.model.shop.Part;
import com.scrap.model.shop.Row;
import com.scrap.service.bot.ClickerService;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.ScreenService;
import com.scrap.util.Wait;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@Scope("singleton")
public class MyTrader {

	private static final double TAXES = 0.9;

	private static final double PRICE_STEP = 0.02;

	private Double myBuyPrice;

	private Double mySellPrice;

	private int cycleCount;

	@Autowired
	private ClickerService clickerService;

	@Autowired
	private RecogniseService recogniseService;

	@Autowired
	private ScreenService screenService;

	private int availableItems;

	private Interface anInterface;

	@PostConstruct
	private void init() {
		anInterface = new Interface();
		anInterface.init();
		myBuyPrice = null;
		mySellPrice = null;
		cycleCount = 0;
		availableItems = 0;
	}

	public void process(Part part) {
		try {
			log.debug("Process...");
			Double currBuyPrice = part.getBuy().peek().getPrice();
			log.debug("Best buy price {}", currBuyPrice);
			Double currSellPrice = part.getSell().peek().getPrice();
			log.debug("Best sell price {}", currSellPrice);
			if (Objects.nonNull(currBuyPrice) && Objects.nonNull(currSellPrice)) {

				log.debug("Profit {}", currSellPrice * TAXES - currBuyPrice);

				if (Objects.nonNull(myBuyPrice) && !currBuyPrice.equals(myBuyPrice)) {
					checkMyBuyOffers();
				}

				if (Objects.nonNull(mySellPrice) && !currSellPrice.equals(mySellPrice)) {
					checkMySellOffers();
				}

				if (currSellPrice * TAXES - currBuyPrice >= 1 &&
						Objects.isNull(mySellPrice) &&
						Objects.isNull(myBuyPrice)) {
					myBuyPrice = currBuyPrice + PRICE_STEP;
					buy(myBuyPrice);
				}

				if (Objects.nonNull(myBuyPrice) &&
						Objects.isNull(mySellPrice) &&
						currSellPrice * TAXES - myBuyPrice > 0 &&
						availableItems > 0) {
					mySellPrice = currSellPrice - PRICE_STEP;
					sell(mySellPrice);
				}

				if (cycleCount == 10) {
					anInterface.getMarket().getPartTab().getAccept().click(clickerService);
					cycleCount = 0;
				}
				log.debug("available items {}", availableItems);
			}
		} catch (Exception e) {
			e.printStackTrace();
			cycleCount++;
			if (cycleCount == 10) {
				anInterface.getMarket().getPartTab().getAccept().click(clickerService);
				cycleCount = 0;
			}
			refresh();
		}
		cycleCount++;
		refresh();
	}

	private void sell(double price) {
		log.debug("Sell {}", price);
		anInterface.getMarket().getPartTab().getSellMenuBar().click(clickerService);
		Wait.mill(100);
		clickerService.print(String.valueOf(price));
		Wait.mill(200);
		anInterface.getMarket().getPartTab().getAcceptSell().click(clickerService);
		Wait.mill(1200);
		anInterface.getMarket().getPartTab().getAccept().click(clickerService);
	}

	private void checkMyBuyOffers() {
		try {
			anInterface.getMarketButton().click(clickerService);
			Wait.mill(100);
			anInterface.getMarket().getPartsTab().getMyOffers().click(clickerService);
			Wait.mill(1000);
			BufferedImage image = screenService.getScreenShot();
			if (Objects.nonNull(anInterface.getMarket().getMyOffersTab().getOrders1().getValue(image, recogniseService)) ||
					Objects.nonNull(anInterface.getMarket().getMyOffersTab().getPurchasePrice1().getValue(image, recogniseService))) {
				log.debug("Someone interrupted my buy price, perform revert");
				anInterface.getMarket().getMyOffersTab().getChoice().click(clickerService);
				Wait.mill(50);
				anInterface.getMarket().getMyOffersTab().getCancel().click(clickerService);
				Wait.mill(100);
				myBuyPrice = null;
			} else availableItems = 1;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			anInterface.getMarket().getMyOffersTab().getChoice().click(clickerService);
			Wait.mill(50);
			anInterface.getMarket().getMyOffersTab().getCancel().click(clickerService);
			myBuyPrice = null;
		}
		anInterface.getMarket().getPartsTab().getParts().click(clickerService);
		Wait.mill(50);
		anInterface.getMarket().getPartsTab().getChoice().click(clickerService);
	}

	private void checkMySellOffers() {
		anInterface.getMarketButton().click(clickerService);
		Wait.mill(100);
		anInterface.getMarket().getPartsTab().getMyOffers().click(clickerService);
		Wait.mill(1000);
		BufferedImage image = screenService.getScreenShot();
		try {
			if (Objects.nonNull(anInterface.getMarket().getMyOffersTab().getOffers1().getValue(image, recogniseService)) ||
					Objects.nonNull(anInterface.getMarket().getMyOffersTab().getSalePrice1().getValue(image, recogniseService))) {
				log.debug("Someone interrupted my sell price, perform revert");
				anInterface.getMarket().getMyOffersTab().getChoice().click(clickerService);
				Wait.mill(50);
				anInterface.getMarket().getMyOffersTab().getCancel().click(clickerService);
				Wait.mill(100);
				mySellPrice = null;
			} else availableItems = 0;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			anInterface.getMarket().getMyOffersTab().getChoice().click(clickerService);
			Wait.mill(50);
			anInterface.getMarket().getMyOffersTab().getCancel().click(clickerService);
			Wait.mill(100);
			mySellPrice = null;
		}
		anInterface.getMarket().getPartsTab().getParts().click(clickerService);
		Wait.mill(50);
		anInterface.getMarket().getPartsTab().getChoice().click(clickerService);
	}

	private void buy(double price) {
		log.debug("Buy {}", price);
		anInterface.getMarket().getPartTab().getBuyMenuBar().click(clickerService);
		Wait.mill(100);
		clickerService.print(String.valueOf(price));
		Wait.mill(100);
		anInterface.getMarket().getPartTab().getAcceptBuy().click(clickerService);
		Wait.mill(1200);
		anInterface.getMarket().getPartTab().getAccept().click(clickerService);
	}

	private double priceDeviation(List<Row> list) {
		double mean = list.stream()
				.mapToDouble(Row::getPrice)
				.average()
				.getAsDouble();
		double meanOfDiffs = list.stream()
				.mapToDouble(element -> StrictMath.pow(element.getPrice() - mean, 2.0))
				.average()
				.getAsDouble();
		log.debug("Price Deviation {}", Math.sqrt(meanOfDiffs));
		return Math.sqrt(meanOfDiffs);
	}

	public void refresh() {
		anInterface.getMarket().getPartTab().getRefresh().click(clickerService);
	}

}
