package com.scrap.model.trader;

import com.scrap.model.shop.Part;
import com.scrap.model.shop.Row;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Stack;

@Slf4j
@Component
@Scope("singleton")
public class Trader {

	private static final int MAX_ELEMENTS_IN_SELL_LIST = 5;

	private static final int MAX_ELEMENTS_IN_BUY_LIST = 5;

	private static final int MAX_ITEMS = 1;

	private static final double MAX_DEVIATION = 0.2;

	private static final double PRICE_STEP = 0.01;

	private static final double MIN_INCOME = 1.0;

	private static final int TAXES = 10;

	private final Stack<Part> data;

	private Queue<Double> sellList;

	private Queue<Double> buyList;

	private int availableItems;

	public Trader() {
		data = new Stack<>();
		sellList = new LinkedList<>();
		buyList = new LinkedList<>();
		availableItems = 0;
	}

	public void process(Part part) {
		data.push(part);
		log.debug("Process...");

		double priceSellAvg = part.getSell().stream()
				.mapToDouble(Row::getPrice)
				.average()
				.getAsDouble();
		log.debug("Price sell avg {}", priceSellAvg);
		double priceBuyAvg = part.getBuy().stream()
				.mapToDouble(Row::getPrice)
				.average()
				.getAsDouble();
		log.debug("Price buy avg {}", priceBuyAvg);

		log.debug("Price Sell - Buy {}", priceSellAvg - priceBuyAvg);

		Double priceSell = part.getSell().peek().getPrice();
		Double mySellPrice = sellList.peek();
		if (Objects.nonNull(priceSell) && Objects.nonNull(mySellPrice) && priceSell > mySellPrice) {
			sellList.poll();
		}
		if (availableItems > 0
				&& (Objects.isNull(mySellPrice) || !Objects.equals(priceSell, mySellPrice))
				&& priceDeviation(part.getSell()) <= MAX_DEVIATION) {
			sell(priceSell);
		}

		Double priceBuy = part.getBuy().peek().getPrice();
		Double myBuyPrice = buyList.peek();
		if (Objects.nonNull(priceBuy) && Objects.nonNull(myBuyPrice) && priceBuy < myBuyPrice) {
			buyList.poll();
			availableItems++;
		}
		if (availableItems < MAX_ITEMS
				&& (Objects.isNull(myBuyPrice) || !Objects.equals(priceSell, mySellPrice))
				&& priceDeviation(part.getBuy()) <= MAX_DEVIATION && priceSell - priceSell * TAXES / 100 - priceBuy >= MIN_INCOME) {
			buy(priceBuy);
		}

	}

	private void sell(Double price) {
		if (MAX_ELEMENTS_IN_SELL_LIST > sellList.size()) {
			Double newPrice = price - PRICE_STEP;
			log.debug("SELL ITEM {}", newPrice);
			sellList.offer(newPrice);
			availableItems--;
		} else {
			sellRevert();
		}
	}

	private void buy(Double price) {
		if (MAX_ELEMENTS_IN_BUY_LIST > buyList.size()) {
			Double newPrice = price + PRICE_STEP;
			log.debug("BUY ITEM {}", newPrice);
			buyList.offer(newPrice);
		} else {
			buyRevert();
		}
	}

	private void sellRevert() {
		log.debug("Revert sell {}", sellList.poll());
	}

	private void buyRevert() {
		log.debug("Revert buy {}", buyList.poll());
	}

	private double priceDeviation(List<Row> list) {
		double mean = list.stream()
				.mapToDouble(Row::getPrice)
				.average()
				.getAsDouble();
		double meanOfDiffs = list.stream()
				.mapToDouble(element -> StrictMath.pow(element.getPrice() - mean, 2.0))
				.average()
				.getAsDouble();
		log.debug("Price Deviation {}", Math.sqrt(meanOfDiffs));
		return Math.sqrt(meanOfDiffs);
	}
}
