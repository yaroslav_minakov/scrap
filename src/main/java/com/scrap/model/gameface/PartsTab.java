package com.scrap.model.gameface;

import com.scrap.model.element.Active;
import com.scrap.model.element.Area;
import com.scrap.model.element.ButtonElement;
import com.scrap.model.element.Initialise;
import com.scrap.model.element.InputElement;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.image.BufferedImage;

@Getter
@Setter
@NoArgsConstructor
public class PartsTab implements Initialise, Active {

	private ButtonElement parts;

	private InputElement search;

	private ButtonElement choice;

	private ButtonElement myOffers;

	@Override
	public void init() {
		parts = new ButtonElement("Parts", new Area(280, 100, 70, 25), RecogniseType.LETTERS,
				new Area(280, 100, 70, 25));
		search = new InputElement("Search", new Area(210, 240, 160, 35), new Area(190, 195, 65, 25));
		choice = new ButtonElement("choice", new Area(205, 380, 300, 60), RecogniseType.UPPER,
				null
		);
		myOffers = new ButtonElement("myOffers", new Area(485, 100, 150, 30), RecogniseType.NOT_DEFINED,
				null
		);
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return parts.isActive(image, recogniseService) && search.isActive(image, recogniseService);
	}

}
