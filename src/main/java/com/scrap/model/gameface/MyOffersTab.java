package com.scrap.model.gameface;

import com.scrap.model.element.Active;
import com.scrap.model.element.Area;
import com.scrap.model.element.ButtonElement;
import com.scrap.model.element.DoubleElement;
import com.scrap.model.element.Initialise;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.image.BufferedImage;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyOffersTab implements Initialise, Active {

	private ButtonElement myOffers;

	private ButtonElement choice;

	private ButtonElement cancel;

	private DoubleElement salePrice1;

	private DoubleElement offers1;

	private DoubleElement purchasePrice1;

	private DoubleElement orders1;

	private DoubleElement salePrice2;

	private DoubleElement offers2;

	private DoubleElement purchasePrice2;

	private DoubleElement orders2;

	@Override
	public void init() {

		myOffers = new ButtonElement("My Offers", new Area(480, 98, 154, 45), RecogniseType.NOT_DEFINED,
				null);
		choice = new ButtonElement("choice", new Area(301, 385, 562, 55), RecogniseType.NOT_DEFINED,
				null);
		cancel = new ButtonElement("cancel", new Area(654, 480, 219, 44), RecogniseType.NOT_DEFINED,
				null);
		salePrice1 = new DoubleElement(new Area(923, 391, 219, 40));
		offers1 = new DoubleElement(new Area(1122, 392, 68, 39));
		purchasePrice1 = new DoubleElement(new Area(1327, 391, 219, 44));
		orders1 = new DoubleElement(new Area(1532, 391, 68, 39));
		salePrice2 = new DoubleElement(new Area(923, 560, 219, 40));
		offers2 = new DoubleElement(new Area(1122, 560, 68, 39));
		purchasePrice2 = new DoubleElement(new Area(1327, 560, 219, 44));
		orders2 = new DoubleElement(new Area(1532, 560, 68, 39));

	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return false;
	}
}
