package com.scrap.model.gameface;


import com.scrap.model.element.Active;
import com.scrap.model.element.Initialise;
import com.scrap.service.bot.RecogniseService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;

@Getter
@Setter
@NoArgsConstructor
@Slf4j
public class Market implements Initialise, Active {

	private PartsTab partsTab;

	private MyOffersTab myOffersTab;

	private PartTab partTab;

	@Override
	public void init() {
		partsTab = new PartsTab();
		myOffersTab = new MyOffersTab();
		partTab = new PartTab();
		partsTab.init();
		myOffersTab.init();
		partTab.init();
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		log.debug("Parts tab {}", partsTab.isActive(image, recogniseService));
		log.debug("Offer tab {}", myOffersTab.isActive(image, recogniseService));
		log.debug("Part tab {}", partTab.isActive(image, recogniseService));
		return partsTab.isActive(image, recogniseService)
				|| myOffersTab.isActive(image, recogniseService)
				|| partTab.isActive(image, recogniseService);
	}
}
