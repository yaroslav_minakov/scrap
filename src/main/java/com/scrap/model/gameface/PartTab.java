package com.scrap.model.gameface;

import com.scrap.model.element.Active;
import com.scrap.model.element.Area;
import com.scrap.model.element.ButtonElement;
import com.scrap.model.element.DoubleElement;
import com.scrap.model.element.Initialise;
import com.scrap.model.element.IntegerElement;
import com.scrap.model.element.RowElement;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Getter
@Setter
@NoArgsConstructor
public class PartTab implements Initialise, Active {

	private static final int HEIGHT = 25;
	private static final int WIDTH = 45;
	private static final int X_COUNT = 490;
	private static final int X_PRICE = 190;
	private static final int Y_STEP = 40;

	private List<RowElement> sellList;

	private List<RowElement> buyList;


	private ButtonElement buyMenuBar;
	private ButtonElement printBuyCount;
	private ButtonElement acceptBuy;

	private ButtonElement sellMenuBar;
	private ButtonElement printSellCount;
	private ButtonElement acceptSell;

	private ButtonElement refresh;

	private ButtonElement accept;

	@Override
	public void init() {
		sellList = new ArrayList<>();
		buyList = new ArrayList<>();
		IntStream.range(0, 4).forEach(index -> {
			sellList.add(new RowElement(new DoubleElement(new Area(X_PRICE, 285 + index * Y_STEP, WIDTH, HEIGHT)),
					new IntegerElement(new Area(X_COUNT, 285 + index * Y_STEP, WIDTH, HEIGHT))));
			buyList.add(new RowElement(new DoubleElement(new Area(X_PRICE, 615 + index * Y_STEP, WIDTH, HEIGHT)),
					new IntegerElement(new Area(X_COUNT, 615 + index * Y_STEP, WIDTH, HEIGHT))));
		});

		buyMenuBar = new ButtonElement("BUY...", new Area(270, 500, 200, 50), RecogniseType.UPPER,
				new Area(345, 510, 80, HEIGHT)
		);
		sellMenuBar = new ButtonElement("SELL...", new Area(270, 830, 200, 50), RecogniseType.UPPER,
				new Area(345, 840, 80, HEIGHT)
		);
		refresh = new ButtonElement("Refresh", new Area(1480, 850, 200, 40), RecogniseType.NOT_DEFINED,
				null
		);
		printBuyCount = new ButtonElement("printBuyCount", new Area(1093, 551, 120, 20), RecogniseType.NOT_DEFINED,
				null
		);
		acceptBuy = new ButtonElement("acceptBuy", new Area(622, 741, 215, 35), RecogniseType.NOT_DEFINED,
				null
		);
		acceptSell = new ButtonElement("acceptSell", new Area(622, 741, 215, 35), RecogniseType.NOT_DEFINED,
				null
		);
		accept = new ButtonElement("accept", new Area(855, 481, 208, 41), RecogniseType.NOT_DEFINED,
				null
		);
		printSellCount = new ButtonElement("printSellCount", new Area(1102, 641, 130, 21), RecogniseType.NOT_DEFINED,
				null
		);
		printSellCount = new ButtonElement("printSellCount", new Area(619, 737, 207, 42), RecogniseType.NOT_DEFINED,
				null
		);
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return sellMenuBar.isActive(image, recogniseService) && buyMenuBar.isActive(image, recogniseService);
	}


}
