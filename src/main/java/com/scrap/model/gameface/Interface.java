package com.scrap.model.gameface;


import com.scrap.model.element.Active;
import com.scrap.model.element.Area;
import com.scrap.model.element.ButtonElement;
import com.scrap.model.element.Initialise;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.image.BufferedImage;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Interface implements Initialise, Active {

	private Market market;

	private ButtonElement marketButton;

	@Override
	public void init() {
		market = new Market();
		market.init();
		marketButton = new ButtonElement("Market", new Area(400, 20, 105, 35), RecogniseType.LETTERS,
				new Area(400, 20, 105, 25)
		);
	}

	@Override
	public boolean isActive(BufferedImage image, RecogniseService recogniseService) {
		return market.isActive(image, recogniseService);
	}
}
