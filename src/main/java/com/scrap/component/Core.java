package com.scrap.component;

import com.scrap.model.gameface.Interface;
import com.scrap.model.shop.Part;
import com.scrap.model.shop.PartAdapter;
import com.scrap.model.trader.MyTrader;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.ScreenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.util.Objects;

@Slf4j
@Component
@Scope("singleton")
public class Core {

	@Autowired
	private PartAdapter partAdapter;

	@Autowired
	private ScreenService screenService;

	@Autowired
	private RecogniseService recogniseService;

	@Autowired
	private MyTrader trader;

	private Interface anInterface;

	@Autowired
	private void init() {
		anInterface = new Interface();
		anInterface.init();
	}

	@Scheduled(fixedDelay = 1000)
	public void process() throws NumberFormatException {
		BufferedImage image = screenService.getScreenShot();
		if (anInterface.getMarket().isActive(image, recogniseService)) {
			//log.debug("Market is active");
			Part part = partAdapter.adapt(anInterface.getMarket().getPartTab(), screenService.getScreenShot());
			if (Objects.nonNull(part)) {
				trader.process(part);
			}
		} else {
			//log.debug("Market inactive");
		}
	}
}
