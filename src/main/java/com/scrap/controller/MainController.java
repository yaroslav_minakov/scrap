package com.scrap.controller;

import com.scrap.service.bot.ClickerService;
import com.scrap.service.bot.RecogniseService;
import com.scrap.service.bot.RecogniseType;
import com.scrap.service.bot.ScreenService;
import com.scrap.util.Wait;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.awt.image.BufferedImage;

@RestController
@AllArgsConstructor
public class MainController {

	private ScreenService screenService;

	private RecogniseService recogniseService;

	private ClickerService clickerService;

	@GetMapping("/bot")
	@ResponseBody
	public ResponseEntity getTest() {
		BufferedImage image = screenService.getScreenShot();
		return ResponseEntity.ok(recogniseService.recognise(image));
	}

	@GetMapping("/recogniseArea")
	public ResponseEntity<String> recogniseArea(@RequestParam Integer x, Integer y, Integer width, Integer height) {
		return ResponseEntity.ok(recogniseService.recognise(screenService.getScreenShot(x, y, width, height), RecogniseType.UPPER));
	}

	@GetMapping("/recogniseAreaNumber")
	public ResponseEntity<String> oneNumber(@RequestParam Integer x, Integer y, Integer width, Integer height) {
		return ResponseEntity.ok(recogniseService.recognise(screenService.getScreenShot(x, y, width, height)));
	}

	@GetMapping("/clicker")
	@ResponseBody
	public String getClick() {
		double s = 1.01;
		clickerService.moveTo(200, 50);
		Wait.sec(1);
		clickerService.leftClick().print(String.valueOf(s));
		Wait.sec(1);
		clickerService.enter();

//		clickerService.moveTo(50, 50);
//		Wait.sec(1);
//		clickerService.moveTo(1870, 50);
//		Wait.sec(1);
//		clickerService.moveTo(50, 1030);
//		Wait.sec(1);
//		clickerService.moveTo(1870, 1030);
//		Wait.sec(1);
//		clickerService.moveTo(910, 540);
		return "test mouse clicker";
	}
}
