package com.scrap.controller;

import com.scrap.service.bot.ScreenService;
import com.scrap.util.Wait;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

@RestController
@AllArgsConstructor
public class ImageController {

	private ScreenService screenService;

	@RequestMapping(value = "/image", method = RequestMethod.GET, produces = "image/jpg")
	@ResponseBody
	public byte[] getTest() {
		Wait.sec(3);
		BufferedImage image = screenService.getScreenShot();
		ByteArrayOutputStream bao = screenService.getImage(image);
		return bao.toByteArray();
	}

	@RequestMapping(value = "/area", method = RequestMethod.GET, produces = "image/jpg")
	@ResponseBody
	public byte[] getTest(@RequestParam Integer x, Integer y, Integer width, Integer height) {
		BufferedImage image = screenService.getScreenShot(x, y, width, height);
		ByteArrayOutputStream bao = screenService.getImage(image);
		return bao.toByteArray();
	}
}
